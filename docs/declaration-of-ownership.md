# Declaration of Personal Ownership

Slave hereby declares themselves a personal property of the Master on their own free will without any pressure, obligation or threat to sign this document.

## 0. Legality
This is currently a non-legally binding documment is created on request of Kreyren's partners.

This documment is created with the best intention to be legal, any pointed out issues tracked in https://git.dotya.ml/kreyrenism/kreyrenism/issues will be processed and investigated for actionability.

## 1. Terms and conditions

### 1.A. Obidience

Slave's obidience is mandatory for an ownership, thus slave is required to bring honor, respect and class to the master through their behavior in public and private.
Disobidience will result in punishment which may be specified below or in termination of this contract.

### 1.B. Life

Slave's life is personal property of the master.

Slave is not allowed to kill themselves without master's permission.

### 1.C. Hygiene

Slave is required to maintain their personal hygiene at all times.

### 1.D. Body physique

Slave is required to maintain peek body physique.

### 1.E. Diet

Slave is required to maintain healthy diet.

## 2. Transmittable diseases

Slave is not allowed to infect the master with any disease.

Slave is required to disclose all of their known diseases to the master.

Slave is required to be tested on these diseases in specified intervals:
- Sexually Transmittable Diseases | After an event capable of transmitting such disease with a non-slave of a master e.g. sexual intercourse

## 3. Punishments

TBD

## 4. Termination

Master can terminate this contract at any time for any reason.